## Kosmos / IT-Core HackOmation 2020 Final Projects 


### Innov8rs

####[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/innov8rs)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQELHq_jAQ2ab9SXZKaY2vn0tx0dn_9-rS5jSslQwP_CHikXuUdlB6WmeVTcdPPlT56Hx_nnXE7ZvPj/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Green Legacy

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/greenlegacy)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRcXejJYm-z4fGuHpDmHzOBnZdoJ2oAoHBQqvFR0ICuts6g-xStm4AbrTWYvsUc0VlZcvFdzNNfz6M6/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Code Docs

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/codedocs)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQKA_Bt0RomsC8oZ6xnTEiCh9i62YlZHDZkzW7UojLFvfwwe8q2a4Oz6IolcnIITw/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Morpheus

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/morpheus)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRNkLCcJJb8AIBWlXx5DUcMp_9j7IZW5JdVATsuwWlDN3m-0Ggh2twpKXdgUXYKnw/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### T-Transparent

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/t-transparent)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQGNmdd0EEOR393DJiJKun_3o8MAK2CUB3_TYnJjwyVSi-jDGWFHNvUcd8eHXYVjQ/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Bitsplease

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/bits-please)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT1jx3C-fvp062kVhtr9St8FG4FiMrOn6Ui--AEYtMVd3-pbStDfuCYWApD7wdnZ8WsAbZsVIbrl3ak/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Beamsafun

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/beamsafun)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRMuDcnbQPU0bo202OjnBx2pRj6gd8GcpIWOirrRssPGMUh-AYFu0Ozjmgk7qOyH8srGhHU3xiKdydN/embed?start=false&loop=false&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Mech Tech

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/mech-tech-students)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS4dIGhN2Bmq5Mp4NY1aeQEtTmYF6DupQ8H0Kvw2XfdnnrXdH4XQvCpM1_sgdU94WLbfWtGyirjrvqd/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Leadhers

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/leadhers)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTfNSb_M08lSOH99pZo_HBu6FA10tJ8nMg0VFuF9G1nKQ0SJEOeichOuwl2SX-4npNdhWmXDqmAF5zM/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Handzoff

[LINK TO DOCUMENTATION ](https://hackomation1.gitlab.io/2020/hackers/handzoff)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTDuIMsH7YXUwZxdhCAAq_Q8PPWL66VIsQWplqy5BgzvwmC6K_E7QM4a5b2AfCQRi2UGHpeH44UQetX/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
